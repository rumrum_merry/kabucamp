import Sample from './lib/Sample';
import $ from 'jquery';
import 'jquery-inview';

$(function() {
	$('.js_scroll_on').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('js_scroll_off');
		}
	});
});
